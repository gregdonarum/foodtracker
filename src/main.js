import { createApp } from 'vue'

import 'bootstrap/dist/css/bootstrap.css'
import * as bootstrap from 'bootstrap/dist/js/bootstrap.bundle'
window.bootstrap = bootstrap;

import App from './App.vue'

const app = createApp(App)
app.mount('#app')
