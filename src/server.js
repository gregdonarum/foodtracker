const express = require('express');
const bodyParser = require('body-parser');
const app = express();
var cors = require('cors')
app.use(cors()) 
const port = 3000;

app.use(bodyParser.json());

let foods = [];

app.get('/foods', (req, res) => {
  res.json(foods);
});

app.post('/foods', (req, res) => {
  const food = req.body;
  foods.push(food);
  res.json(food);
});

app.listen(port, () => {
  console.log(`Server running at http://localhost:${port}`);
});
